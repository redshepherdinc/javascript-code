![logo](https://redshepherdmedia.s3-us-west-2.amazonaws.com/red_logo_64.png "logo")

# Red Shepherd Javascript API Docs

## _Live Demos on Codepen_

> **_[Click to Open ||| Entire Codepen API COLLECTION](https://codepen.io/collection/AWpBMg)_**

---

> **_[Click to Open ||| Card Payment Demo on Codepen](https://codepen.io/qmar/full/mWxVXW)_**

> **_[Click to Open ||| ACH Payment Demo on Codepen](https://codepen.io/qmar/full/GdBazY)_**

---

![demos](https://redshepherdmedia.s3-us-west-2.amazonaws.com/javascript_api_demo.png "Demos")

---

## _Follow these steps for Simple API Integration_

---

### Step 1 - Adding the payment library

> **_Add the payment library to your script section_**

> **_Create a Redpay API object which lets you access all the payment functions_**

**_Script_**

```html
<!-- Red Shepherd Encrypted card processor library -->
<script
  type="text/javascript"
  src="https://www.redshepherd.com/cdn/3.0.2/js/redpay.js"
/>

<script>
  /*** To keep it simple, This demo script places the RedPay object on the window ***/
  /*** You can store it using your own logic to store global variables ***/

  /*** REPLACE app, url and key with your PROD keys and use a valid account ***/
  /*** These are DEMO Keys which you can safely use for testing ***/
  let app = "DEMO";
  let endpoint = "https://redpaystable.azurewebsites.net/";
  let key =
    "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=";

  /*** Initialize and assign RedPay object to window  ***/
  window.RedPay = new RedPay(app, key, endpoint);
</script>
```

---

### Step 2 a - Charge a Credit / Debit Card

> **_[Click to Open ||| Card Payment Demo on Codepen](https://codepen.io/qmar/full/mWxVXW)_**

> **_For Credit / Debit Card Payments Add this function to your script section_**

**_Script_**

```javascript
<script>
function chargeCard(req) {
  if(window.RedPay) {
    window.RedPay.ChargeCard(req).then(function(res) {
      console.log(JSON.stringify(res, null, 4));
    }, function(err) {
      console.error(JSON.stringify(err, null, 4));
    });
  } else {
    console.warn('RedPay is not defined');
  }
}
</script>
```

**_ChargeCard Request Example_**

```javascript
let req = {
  account: "4111111111111111",
  amount: 2000,
  expmmyyyy: "122023",
  cvv: "123",
  cardHolderName: "John Smith",
  avsZip: "10001",
};
```

**_ChargeCard Response_**

```javascript
{
  responseCode: "A",
  transactionId: "DEMO.j1kbwwwh531ivcsh",
  authCode: "PPS531",
  token: "9418594164541111",
  cardLevel: "Q",
  cardBrand: "V",
  cardType: "C",
  processorCode: "CC",
  app: "DEMO",
  account: "9418594164541111",
  cardHolderName: "John Smith",
  amount: 2000,
  timeStamp: "12/10/2020 1:46:47 AM",
  text: "Approval Approval",
  ipAddress: "198.54.106.248:51554",
  avsCode: "Z"
}
```

---

### Step 2 b - For ACH Payments

> **_[Click to Open ||| ACH Payment Demo on Codepen](https://codepen.io/qmar/full/GdBazY)_**

> **_Add this function to your script section_**

**_Script_**

```javascript
<script>
function chargeACH(req) {
  if(window.RedPay) {
    window.RedPay.ChargeACH(req).then(function(res) {
      console.log(JSON.stringify(res, null, 4));
    }, function(err) {
      console.error(JSON.stringify(err, null, 4));
    });
  } else {
    console.warn('RedPay is not defined');
  }
}
</script>
```

**_ChargeACH Request Example_**

```javascript
let req = {
  account: "1234567890",
  accountType: "C",
  routing: "121122676",
  amount: 2100,
  cardHolderName: "Jill Smith",
};
```

**_ChargeACH Response_**

```javascript
{
  responseCode: "A",
  transactionId: "DEMO.b4gxvbwexynb8u3i",
  authCode: "SRZ79B",
  token: "9120988649567890",
  processorCode: "CC",
  app: "DEMO",
  account: "9120988649567890",
  amount: 2100,
  timeStamp: "12/10/2020 2:06:35 AM",
  text: "Success Success",
  ipAddress: "198.54.106.248:56473",
  avsCode: "U"
}
```

---

### Step 3 - Important notes

> **_IMPORTANT NOTE ||| API Amount is in cents ie 20.00 = 2000, 21.56 = 2156_**

> **_Contact us for your Production API payment keys_**
> Email us [support@redshepherd.com](mailto:support@redshepherd.com) or go to [https://dashboard.redshepherd.com/signup](https://dashboard.redshepherd.com/signup)

---

---

---

## _Other API Functions_

---

### Get a Transaction

> **_[Click to Open ||| Get Transaction Demo on Codepen](https://codepen.io/qmar/full/jOMaWNj)_**

> **_Retrieve existing Transaction from the API_**

**_Script_**

```javascript
<script>
function getTransaction(req) {
  if(window.RedPay) {
    window.RedPay.GetTransaction(req).then(function(res) {
      console.log(JSON.stringify(res, null, 4));
    }, function(err) {
      console.error(JSON.stringify(err, null, 4));
    });
  } else {
    console.warn('RedPay is not defined');
  }
}
</script>
```

**_GetTransaction Request Example_**

```javascript
let req = {
  transactionId: "DEMO.v9xnqezrwabwl1h7",
};
```

**_GetTransaction Response_**

```javascript
{
  transactionId: "DEMO.v9xnqezrwabwl1h7",
  app: "DEMO",
  requestAmount: 0,
  responseAmount: 0,
  isVoided: false,
  isRefunded: false,
  responseCode: "A",
  userid: "STABLE",
  timestamp: "2020-12-24T21:28:07.841Z",
  ipaddress: "198.54.106.248:54568",
  processor: "CC"
}
```

---

### Tokenize a Credit / Debit Card

> **_[Click to Open ||| Tokenize Card Demo on Codepen](https://codepen.io/qmar/full/mdrOEWe)_**

> **_Tokenize a card with us and process future payments just using the token, this is useful in saving Customer's payment information with us securely and using it to charge their account for future orders, Save the token securely on your side and tie it to the Customer's payment profile. DO NOT make this token public / visible anywhere on your UI or App_**

**_Script_**

```javascript
<script>
function tokenizeCard(req) {
  if(window.RedPay) {
    window.RedPay.TokenizeCard(req).then(function(res) {
      console.log(JSON.stringify(res, null, 4));
    }, function(err) {
      console.error(JSON.stringify(err, null, 4));
    });
  } else {
    console.warn('RedPay is not defined');
  }
}
</script>
```

**_TokenizeCard Request Example_**

```javascript
let req = {
  account: "4111111111111111",
  expmmyyyy: "122023",
  cvv: "123",
  cardHolderName: "John Smith",
  avsZip: "10001",
};
```

**_TokenizeCard Response_**

```javascript
{
  responseCode: "A",
  transactionId: "DEMO.ictm5y9ppew6rk5r",
  authCode: "PPS405",
  token: "9418594164541111",
  cardLevel: "Q",
  cardBrand: "V",
  cardType: "C",
  processorCode: "CC",
  app: "DEMO",
  account: "9418594164541111",
  cardHolderName: "John Smith",
  amount: 2000,
  timeStamp: "12/10/2020 7:09:01 PM",
  text: "Approval Approval",
  ipAddress: "172.58.140.121:38164",
  avsCode: "Z"
}
```

---

### Tokenize ACH Bank Account

> **_[Click to Open ||| Tokenize ACH Account Demo on Codepen](https://codepen.io/qmar/full/vYXyKWN)_**

> **_Tokenize a Customer's ACH Account with us and process future payments just using the token, Save the token securely on your side and tie it to the Customer's payment profile. DO NOT make this token public / visible anywhere on your UI or App_**

**_Script_**

```javascript
<script>
function tokenizeACH(req) {
  if(window.RedPay) {
    window.RedPay.TokenizeACH(req).then(function(res) {
      console.log(JSON.stringify(res, null, 4));
    }, function(err) {
      console.error(JSON.stringify(err, null, 4));
    });
  } else {
    console.warn('RedPay is not defined');
  }
}
</script>
```

**_TokenizeACH Request Example_**

```javascript
let req = {
  account: "1234567890",
  accountType: "C",
  routing: "121122676",
  cardHolderName: "Jill Smith",
};
```

**_TokenizeACH Response_**

```javascript
{
  responseCode: "A",
  token: "9hcuqcrw5kg6g2o4",
  app: "DEMO",
  account: "7890",
  cardHolderName: "John Smith",
  amount: 0,
  timeStamp: "12/10/2020 7:03:46 PM",
  text: "APPROVED",
  ipAddress: "172.58.140.121:51983"
}
```

---

### Charge a Token

> **_[Click to Open ||| Charge Token Demo on Codepen](https://codepen.io/qmar/full/yLaVJjE)_**

> **_Charge an existing token by passing in the token and the amount_**

**_Script_**

```javascript
<script>
function chargeToken(req) {
  if(window.RedPay) {
    window.RedPay.ChargeToken(req).then(function(res) {
      console.log(JSON.stringify(res, null, 4));
    }, function(err) {
      console.error(JSON.stringify(err, null, 4));
    });
  } else {
    console.warn('RedPay is not defined');
  }
}
</script>
```

**_ChargeToken Request Example_**

```javascript
let req = {
  token: "9hcuqcrw5kg6g2o4",
  amount: 2200,
};
```

**_ChargeToken Response_**

```javascript
{
  responseCode: "A",
  transactionId: "DEMO.p97sw7y55qive1ub",
  authCode: "2F5F9B",
  token: "9120988649567890",
  processorCode: "CC",
  app: "DEMO",
  account: "9120988649567890",
  amount: 2200,
  timeStamp: "12/10/2020 7:12:30 PM",
  text: "Success Success",
  ipAddress: "172.58.140.121:61468",
  avsCode: "U"
}
```

---

### Void a Transaction

> **_[Click to Open ||| Void Transaction Demo on Codepen](https://codepen.io/qmar/full/ZEpBOjq)_**

> **_Voiding a transaction will cancel an existing unsettled transaction, transactions normally settle in 24 hours so you should use Void function to refund the customer his money if the transaction occured in the last 24 hours, ACH Transactions might take longer to settle and voiding an ACH transaction will depend on the settlement period for that particular ACH._**
>
> > A good rule of thumb is that if you did not actually see the deposit from that transaction in your merchant bank account that means that the transaction is still not settled and can be voided.

**_Script_**

```javascript
<script>
function voidTransaction(req) {
  if(window.RedPay) {
    window.RedPay.Void(req).then(function(res) {
      console.log(JSON.stringify(res, null, 4));
    }, function(err) {
      console.error(JSON.stringify(err, null, 4));
    });
  } else {
    console.warn('RedPay is not defined');
  }
}
</script>
```

**_Void Request Example_**

```javascript
let req = {
  transactionId: "DEMO.b4gxvbwexynb8u3i",
};
```

**_Void Response_**

```javascript
{
  responseCode: "A",
  transactionId: "DEMO.ictm5y9ppew6rk5r",
  processorCode: "CC",
  app: "DEMO",
  amount: 2000,
  timeStamp: "12/10/2020 7:16:09 PM",
  text: "Approval Approval",
  ipAddress: "172.58.140.121:65012"
}
```

---

### Refund a Transaction

> **_[Click to Open ||| Refund Transaction Demo on Codepen](https://codepen.io/qmar/full/mdrOEaO)_**

> **_Refunding a transaction will cancel an existing settled transaction, transactions normally settle in 24 hours so you should use Refund function to refund the customer his money if the transaction occured more than a day in the past. I.e. the transaction has settled and you have already received the money in your merchant bank account._**

**_Script_**

```javascript
<script>
function refundTransaction(req) {
  if(window.RedPay) {
    window.RedPay.Refund(req).then(function(res) {
      console.log(JSON.stringify(res, null, 4));
    }, function(err) {
      console.error(JSON.stringify(err, null, 4));
    });
  } else {
    console.warn('RedPay is not defined');
  }
}
</script>
```

**_Refund Request Example_**

```javascript
let req = {
  transactionId: "DEMO.b4gxvbwexynb8u3i",
};
```

**_Refund Response_**

```javascript
{
  responseCode: "A",
  transactionId: "DEMO.b4gxvbwexynb8u3i",
  processorCode: "CC",
  app: "DEMO",
  amount: 2000,
  timeStamp: "12/10/2020 7:16:09 PM",
  text: "Approval Approval",
  ipAddress: "172.58.140.121:65012"
}
```

---

> © Copyright 2020 Red Shepherd Inc.
